//
//  Pokemon.swift
//  mentoryProject
//
//  Created by Satoritech 23 on 11/08/23.
//

import Foundation

struct Pokemon: Decodable {
    let name: String
    let url: String
    
    enum CodingKeys:String,CodingKey {
        case name,url
    }
}
