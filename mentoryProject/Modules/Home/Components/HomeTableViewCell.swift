//
//  HomeTableViewCell.swift
//  mentoryProject
//
//  Created by Satoritech 23 on 31/07/23.
//

import UIKit
protocol HomeTableViewCellDelegate {
    func greetings()
}


class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var lblQuestions: UILabel!
    @IBOutlet weak var viewCard: UIView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnArrow: UIImageView!
    
    var delegate: HomeTableViewCellDelegate?
    var closure: (()->Void)? = { print("NADA DE NADA")}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupGestures()
    }
    
    private func setupGestures(){
        viewCard.isUserInteractionEnabled = true
        viewCard.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onClickedCard(tapGestureRecognizer:))))
        
        btnArrow.isUserInteractionEnabled = true
        btnArrow.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onClickedArrow(_:))))
    }
    
    @objc func onClickedCard(tapGestureRecognizer: UITapGestureRecognizer) {
        delegate?.greetings()
    }
    
    @objc func onClickedArrow(_ sender: Any) {
        closure?()
    }
    
    func setupFor(data: Pokemon){
        lblQuestions.text = data.name
        lblDescription.text = data.name
    }
    
}
