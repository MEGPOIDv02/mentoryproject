//
//  HomeInteractor.swift
//  mentoryProject
//
//  Created by Satoritech 23 on 26/07/23.
//  
//

import Foundation

class HomeInteractor: HomeInteractorInputProtocol {
    weak var presenter: HomeInteractorOutputProtocol?
    
    func getPokemons() {
        RemoteRequests.requestGet(url: ApiEnvironment.current.getURLFor(endpoint: .pokemons), completion: {( result : Result<GeneralArrayResponsePokemon<Pokemon>,ServiceError>) in
                switch result{
                case .success(let data):
                    self.presenter?.requestPokemons(data: data)
                case .failure(let error):
                    print("ERROR: \(error)")
                }
        })
    }
    
}
