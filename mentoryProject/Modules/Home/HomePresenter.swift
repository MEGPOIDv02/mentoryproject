//
//  HomePresenter.swift
//  mentoryProject
//
//  Created by Satoritech 23 on 26/07/23.
//  
//

import Foundation

class HomePresenter {
    //MARK: Properties
    var interactor: HomeInteractorInputProtocol?
    weak private var view: HomeViewProtocol?
    private let router: HomeRouterProtocol
    
    
    init(interface: HomeViewProtocol, interactor: HomeInteractorInputProtocol, router: HomeRouterProtocol) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }
}

extension HomePresenter: HomePresenterProtocol {
    func getPokemons() {
        interactor?.getPokemons()
    }
    
    
}

extension HomePresenter: HomeInteractorOutputProtocol {
    func requestPokemons(data: GeneralArrayResponsePokemon<Pokemon>) {
        if let pokemons = data.results {
            view?.requestPokemons(data: pokemons)
        }else{
            view?.requestPokemons(data: [])
        }
        
    }
    
    
}
