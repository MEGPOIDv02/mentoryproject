//
//  HomeProtocols.swift
//  mentoryProject
//
//  Created by Satoritech 23 on 26/07/23.
//  
//

import Foundation

//View
protocol HomeViewProtocol: AnyObject {
    var presenter: HomePresenterProtocol? {get set}
    func requestPokemons(data: [Pokemon])
}

//Interactor
protocol HomeInteractorInputProtocol: AnyObject {
    //Presenter -> Interactor
    var presenter: HomeInteractorOutputProtocol? {get set}
    
    func getPokemons()
}


protocol HomeInteractorOutputProtocol: AnyObject {
    //Interactor->Presenter
    func requestPokemons(data: GeneralArrayResponsePokemon<Pokemon>)
}

//Presenter
protocol HomePresenterProtocol: AnyObject {
    var interactor: HomeInteractorInputProtocol? {get set}
    func getPokemons()
}


//Router
protocol HomeRouterProtocol: AnyObject {
    
}
