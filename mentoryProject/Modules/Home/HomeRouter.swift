//
//  HomeRouter.swift
//  mentoryProject
//
//  Created by Satoritech 23 on 26/07/23.
//  
//

import Foundation
import UIKit

class HomeRouter {
    weak var viewController: UIViewController?

    static func createModule()->UIViewController{

        let view = HomeViewController(nibName: "HomeViewController", bundle: nil)

        let interactor = HomeInteractor()
        let router = HomeRouter()
        
        let presenter = HomePresenter(interface: view, interactor: interactor, router: router)
        
        
        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view
        
        return view
    }
    
}

extension HomeRouter: HomeRouterProtocol {
    
}
    
    
    

