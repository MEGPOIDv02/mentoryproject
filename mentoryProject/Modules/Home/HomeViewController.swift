//
//  HomeViewController.swift
//  mentoryProject
//
//  Created by Satoritech 23 on 26/07/23.
//  
//

import UIKit

class HomeViewController: UIViewController {
    var presenter: HomePresenterProtocol?
    
    
    @IBOutlet weak var table: UITableView!
    
    private var arrayPokemons: [Pokemon] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.getPokemons()
    }
    
    private func setupTable(){
        table.dataSource = self
        table.delegate = self
        table.register(UINib(nibName: "HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeTableViewCell")
        table.separatorStyle = .none
    }
}

extension HomeViewController: HomeViewProtocol {
    
    func requestPokemons(data: [Pokemon]) {
        self.arrayPokemons = data
        DispatchQueue.main.async {
            self.table.reloadData()
        }
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayPokemons.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for : indexPath) as! HomeTableViewCell
        cell.delegate = self
        cell.setupFor(data: arrayPokemons[indexPath.row])
        cell.closure = {
            print("Saludos desde Closure")
        }
        
        return cell
    }
    
    
}

extension HomeViewController: HomeTableViewCellDelegate{
    func greetings() {
        print("Saludos desde Protocolo")
    }
    
    
}
