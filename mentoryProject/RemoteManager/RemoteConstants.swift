//
//  RemoteConstants.swift
//  mentoryProject
//
//  Created by Satoritech 23 on 07/08/23.
//

import Foundation

enum ApiEnvironment: String {
    case development = "https://pokeapi.co"
    case production = ""
    
    
    static let current: ApiEnvironment = .development
    
    func getURLFor(endpoint: ApiEndpoint) -> String {
        return "\(self.rawValue)/api/v2/\(endpoint.rawValue)"
    }
    
    func getURLBase() -> String {
        return "\(self.rawValue)"
    }
}

enum ApiEndpoint: String {
    case pokemons = "pokemon?limit=100&offset=0"
}
