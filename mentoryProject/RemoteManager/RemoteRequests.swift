//
//  RemoteRequests.swift
//  mentoryProject
//
//  Created by Satoritech 23 on 07/08/23.
//

import Foundation
import Alamofire
import PKHUD
import PDFKit
enum ServiceError: Error {
    case decodingError(description: String)
}

class RemoteRequests {
    //Aprobado
    public static func requestGet<T>(url: String, completion: @escaping(_ response : Result<T, ServiceError>) -> Void) where T: Decodable {
        AF.request(url, method: .get,headers: getHeaders()).responseDecodable { (response: DataResponse<T, AFError>) in
            RemoteRequests.validateResponse(response, completion: completion)
        }
    }
    
    public static func requestPost<T>(url: String, object: Encodable, completion: @escaping(_ data: Result<T, ServiceError>) -> Void) where T: Decodable {
        
        let body = object.dictionary!
        AF.request(url, method: .post, parameters: body as Parameters, encoding: JSONEncoding.default,
                   headers: RemoteRequests.getHeaders()).responseDecodable { (response: DataResponse<T, AFError>) in
            RemoteRequests.validateResponse(response, completion: completion)
        }
    }
    
   
    
    //-------------------------------------------------------------with payload
    
    public static func requestPostFiles<T>(url:String,images:[String:UIImage]=[:],files:[String:PDFDocument]=[:],object:Encodable,completion:@escaping( _ response:Result<T,ServiceError>)->Void) where T:Decodable{
        let encoder = JSONEncoder()
        if let data = try? encoder.encode(object) {
            
            AF.upload(multipartFormData: { multipart in
                images.forEach({ image in
                    multipart.append(image.value.jpegData(compressionQuality: 0.1)!, withName: image.key, fileName: "\(image.value).jpg", mimeType: "image/jpeg")
                })
                
                files.forEach({ file in
                    if let url=file.value.documentURL{
                        multipart.append(url, withName: file.key, fileName: "\(url)", mimeType: "file/pdf")
                        
                    }
                })
                multipart.append(data, withName: "data")
                
            },
                      to: url, method: .post, headers: RemoteRequests.getHeaders()).responseDecodable { (response: DataResponse<T, AFError>) in
                
                RemoteRequests.validateResponse(response, completion: completion)
            }
            
        }
        
    }
    //Aprobado
    private static func getHeaders() -> HTTPHeaders {
        let defaults = UserDefaults.standard
        let apiToken = defaults.string(forKey: "")
        var headers: HTTPHeaders = []
        
        if let token=apiToken {
            headers.add(HTTPHeader(name: "Authorization", value: "Bearer \(token)"))
        }
        headers.add(HTTPHeader(name: "Accept", value: "application/json"))
        return headers
    }
    
    private static func validateResponse<T>(_ response: DataResponse<T, AFError>, completion: @escaping(_ data: Result<T, ServiceError>) -> Void) where T: Decodable {
        debugPrint(response)
     
        if let code = response.response?.statusCode,code>500 {
            HUD.show(.labeledError(title: "Error", subtitle: "Servicio no disponible"))
        }
        if let error = response.error {
            NSLog("API error: \(String(describing: error))")
            completion(.failure(.decodingError(description: "API error: \(String(describing: error))")))
            return
        }
        
        if let data = response.value {
            completion(.success(data))
        } else {
            completion(.failure(.decodingError(description: "Data invalid")))
        }
    }
    
    private static func convertStringToDictionary(data: Data) -> [String:String]? {
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:String]
            return json
        } catch {
            print("Something went wrong")
        }
        return nil
    }
    private static func convertStringToDictionary(text: String) -> [String:String]? {
        if let data = text.data(using: .utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:String]
                return json
            } catch {
                print("Something went wrong")
            }
        }
        return nil
    }
   
    
}
extension Encodable {
    var dictionary: [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
      }
    
}
