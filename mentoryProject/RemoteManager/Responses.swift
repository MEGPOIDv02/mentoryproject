//
//  Responses.swift
//  mentoryProject
//
//  Created by Satoritech 23 on 07/08/23.
//

import Foundation

struct BasicResponse:Decodable{
    let success:Bool
    let message:String
}

struct GeneralArrayResponse<T:Decodable>:Decodable{
    var success:Bool
    var message:String
    var data:[T]?
}

struct GeneralObjectResponse<T:Decodable>:Decodable{
    var success:Bool?
    var message:String?
    var data:T?
}


struct GeneralArrayResponsePokemon<T:Decodable>:Decodable{
    var results:[T]?
}
